
public class DinamicArray {
	Object[] array = new Object[10];
	int size = 0;
	
	public boolean add(Object obj) {
		if (size == array.length)
			allocateMemory();
		array[size] = obj;
		return true;
	}
	
	private void allocateMemory() {
		Object[] tmpArray = new Object[array.length * 2];
		for (int i = 0; i < array.length; i++) {
			tmpArray[i] = array[i];
		}
		array = tmpArray;
	}
}
