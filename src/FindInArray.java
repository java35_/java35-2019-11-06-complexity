
public class FindInArray {
	public static void main(String[] args) {
		int[] array = {1, 5, 3, 8, 10, 2, 4, 6, 9};
		System.out.println(findElementByIndex(array, 8));
		
		System.out.println(findIndexByValue(array, 2));
		
//		bubbleSort(array);
//		printArray(array);
		
		insertionSort(array);
		printArray(array);
		
		System.out.println(binarySearch(array, 0));
	}

	// 0(logN)
	private static int binarySearch(int[] array, int value) {
		int bottom = 0;
		int upper = array.length;
		int index = (upper - bottom) / 2 + bottom;
		
		while(bottom <= upper) {
			if (value == array[index])
				return index;
			if (value > array[index])
				bottom = index + 1;
			else
				upper = index - 1;
			index = (upper - bottom) / 2 + bottom;
			System.out.println("+");
		}
		return -1;
	}

	// O(n*n)
	private static void insertionSort(int[] array) {
		for (int i = 1; i < array.length; i++) {
			int key = array[i];
			for (int j = i; j > 0; j--) {
				if (key < array[j - 1]) {
					array[j] = array[j - 1];
					array[j - 1] = key;
				}
			}
		}
	}

	// O(n*n)
	private static void bubbleSort(int[] array) {
		for (int i = 1; i < array.length; i++) {
			for (int j = 1; j < array.length; j++) {
				if (array[j - 1] > array[j]) {
					int tmpValue = array[j - 1];
					array[j - 1] = array[j];
					array[j] = tmpValue;
				}
			}
		}
	}

	private static void printArray(int[] array) {
		System.out.println("****************************");
		for (int value : array) {
			System.out.print(value + " ");
		}
		System.out.println();
		System.out.println("****************************");
	}

	// n = 9
	// O(n)
	private static int findIndexByValue(int[] array, int value) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(i + " ");
			if (array[i] == value) {
				System.out.println();
				return i;
			}
		}
		return -1;
	}

	// O(1) - константная сложность
	private static int findElementByIndex(int[] array, int index) {
		return array[index];
	}
}
