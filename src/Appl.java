
public class Appl {
	public static void main(String[] args) {
		int[] array = {1, 5, 3, 8, 10, 2, 4, 6, 9};
		
		array = addElementToArray(array, 15);
		
		printArray(array);
	}

	private static int[] addElementToArray(int[] array, int newValue) {
		int[] tmpArray = new int[array.length + 1];
		for (int i = 0; i < array.length; i++) {
			tmpArray[i] = array[i];
		}
		tmpArray[tmpArray.length - 1] = newValue;
		return tmpArray;
	}
	
	private static void printArray(int[] array) {
		System.out.println("****************************");
		for (int value : array) {
			System.out.print(value + " ");
		}
		System.out.println();
		System.out.println("****************************");
	}

}
